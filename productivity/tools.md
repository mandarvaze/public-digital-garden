## Text-to-speech

* [Natural Reader](https://www.naturalreaders.com/software.html)
  * Free for personal use
  * Supports PDFs, ePubs
  * Allows to download MP3 so that one can make print book into audio book
