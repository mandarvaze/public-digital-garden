## Tips and Tricks

### Manually updating `created` and `updated` front matter
The `created` and `updated` in the front matter are Unix epoch timestamps. For the most part it doesn't matter, and you are unlikely to pay attention to them since they are autopopulated for you.

But I was moving some of my old notes and I wanted to ensure they are timestamped correctly using their original time of creation.~~Kevin pointed me to [this](https://www.epochconverter.com) site where I could get the epoch timestamp for older dates.~~

~~One tiny inconvenience with above site is that the timestamp is only 10 digit - while Dendron uses 13 digit timestamp that includes milliseconds too. So I add 3 extra zeros at the end.
I don't care about millisecond precision, so it is OK 😆~~

*I had to update the `updated` field outside VSCodium using external editor, since Dendron plugin kept updating it with current timestamp* 😄

When creating the note entirely outside VSCode/ium, one can use [CurrentMillis](https://currentmillis.com/)

Turns out [CurrentMillis](https://currentmillis.com/) also lets you convert older dates - It does automatically what I did manually, add extra 3 zeros at the end 😆
### Create note from the terminal

I have had a problem where whenever I run VSCodium (along with Firefox) my Linux machine becomes unresposive. While I am looking at multiple options to resolve this, one thing I decided to use was to Edit the markdown files *elsewhere* like in Emacs (or `nvim`) But Dendron gives a bunch of nice features that I could not do outside VSCodium (Or so I thought)
One of them was to create a daily journal note. Turns out I **can** do that from the terminal using :

`dendron note write --fname "daily.journal.2023.04.30"`

Couple of things to remember though : 

* `dendron-cli` needs to be globally installed via `-g` option
* Don't use `.md` at the end. Else the file with two `.md` gets created. Like `daily.journal.2021.08.31.md.md`
* Optionally, you can specify `--vault "vaultName" --body "this is a body"`


[[Config]]
[[Shortcuts]]
[[Workflows]]
[[Shortcuts]]