## Habit tracker using MD tables

[Reference](https://www.makeuseof.com/tag/create-markdown-table/)

Habit 1 | Habit 2 | Habit 3
--- | --- | ---
❌ | ✅ |✅ |



### Snippet to move tasks forward

In bullet journal, we move the task to the next day using symbol `>`
We can not use it as is, cause `>` has special meaning in markdown, hence it needs to be enclosed in `

It is a chore to type `>` each time, instead create a prefix like 

```json
{
  "`>": {
    "prefix": "forw",
    "body": [
      "`>`"
    ],
    "description": "Move task forward"
  }
}
```

Use this by typing `forw` and then <kbd>Tab</kbd> to insert `>`

While `forw` are 4 letters and `>` is just 3, but somehow `forw` is more logical. 🤷‍♂️
