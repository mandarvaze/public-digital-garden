To create a new note from existing one :

1. Select the text from the existing note
2. `Dendron: Lookup`
3. Type in the name of the new note, <kbd>Enter</kbd>
4. Save both new and old notes 😄
