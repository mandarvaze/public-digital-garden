## [Scanning the sketch notes](https://www.youtube.com/watch?v=QsEam0p_eNo)

* 600 dpi
* In Preview, increase the contrast
* Increase sharpness, useful during vectorization (SVG) later.

## Resources
* [Sketch notes Podcast](http://rohdesign.com/weblog/2015/8/26/the-sketchnote-podcast-season-1.html)
* Find icons for sketchnotes : [The Noun Project](https://thenounproject.com/)
* [PNG to SVG](https://eprev.org/2015/05/27/converting-png-to-svg/)
* 🆕 [IQ Doodle School](https://school.iqdoodle.com/how-to-doodle-tutorials/) - Good ideas for abstract things like Solution, Risk etc. Useful during sketchnoting.

----
*Migrated from [devnotes](https://devnotes.desipenguin.com)*
