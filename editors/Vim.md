## Append a text at the end of several lines

`:%s/$/text to append/`

* You can add `gc` at the end if you want confirmation for each replacement like `:%s/$/text to append/gc`
* To append the text only to some lines say between line 10 to 27 `:10,27s/$/text
  to append/`

Ref : [Stackover Flow](https://stackoverflow.com/a/20833953/154947)

## To add same text at the beginning of several consecutive lines 

1. Visually select a block using <kbd>Ctrl+v</kbd> followed by arrow keys.
2. Without Escaping <kbd>Shift+i</kbd> and type whatever text you want (say `#`
   to comment several lines. You can type as many characters as you need.) Then
   <kbd>Esc</kbd>

## Format text

This is useful when you write long text. When writing, go on typing to put the
thoughts on ~~paper~~ the computer. Later one was use format commands to make
it look better.

> `gqap` : Format the current paragraph

It will make all the lines of `<textwidth>` long by adding a line break where
required.

~~While it does not matter for markdown (since the rendered HTML doesn't care
about line breaks anyway) but~~ when you are looking at the raw markdown, the
long line trouble me.
