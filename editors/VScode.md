## Where does VSCode/VSCodium store info ?

Two places

1. `~/.vscode` (or `~/.vscode-oss` for VSCodium)
2.  Depending on your platform, the user settings file is located here:
    * **Windows** %APPDATA%\Code\ or %APPDATA%\VSCodium
    * **macOS** $HOME/Library/Application Support/Code/ or  $HOME/Library/Application Support/VSCodium/
    * **Linux** $HOME/.config/Code/ or  $HOME/.config/VSCodium/

I needed to cleanup both the locations to wipe clean the traces of VSCode/VSCodium

## Relative Line numbers 

Use the `editor.lineNumbers` setting with the value of `relative`

## Manually installing/updating extensions

Sometimes VSCodium can not connect to the OpenVSX registry (aka Extensions Marketplace)

Most of the time, this is temporary. But in case you can not wait to install new
extension or upgrade existing one, you can download the extension from 
[OpenVSX web site](https://open-vsx.org/) and then drag-and-drop the `.vsx` 
file in the Extensions list.

## Auto format `rust` code on save

Add the following to `settings.json`

```json
"[rust]": {
    "editor.defaultFormatter": "rust-lang.rust-analyzer",
    "editor.formatOnSave": true
}
```
