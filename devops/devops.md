* [Dokku](https://dokku.com/) is your own, single-host version of Heroku.
* [CapRover](https://caprover.com) : Similar to Dokku above, but *seems* to do more
* [Deta.sh](https://www.deta.sh/) : Free cloud hosting for python/node apps

## LetsEncrypt Certbot SSL For Ubuntu

Install certbot using : 

```sh
sudo apt-get install certbot python-certbot-nginx
```

Install the certificate using : (Replace *mydomain.com* with appropriate domain name).

```sh
sudo certbot --nginx -d mydomain.com -d www.mydomain.com
```

This command is so awesome that it : 

1. Configures nginx (or apache) server for you. 
2. Optionally configured nginx (or apache) to redirect all `http` traffic to `https` (You should always choose *Yes* when asked)
3. Configures auto renewal of the certificate. 

It automatically adds an entry into crontab so that certbot is run twice daily. There is an entry under `/etc/cron.d/certbot`

Run the following to check the status of renewal service :

```sh
sudo systemctl status certbot.timer
```

Whats more 

> If the automated renewal process ever fails, Let’s Encrypt will send a message to the email you specified, warning you when your certificate is about to expire.

[[cron]]
[[docker]]
[[monit]]
[[nginx]]
