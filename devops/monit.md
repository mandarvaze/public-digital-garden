> Monit is a small Open Source utility for managing and monitoring Unix systems. Monit conducts automatic maintenance and repair and can execute meaningful causal actions in error situations

## Why won't my config changes take effect ?

After you change the configuration file, one needs to `sudo monit reload` where monit daemon will re-read the configuration file.
Without this, just `sudo monit restart all` does not help. All the processes will be started with old configuration.

