## Install Elasticsearch via Docker

```sh
docker network create elastic
docker pull docker.elastic.co/elasticsearch/elasticsearch:8.2.2
docker run --name es01 --net elastic -p 9200:9200 -p 9300:9300 -it docker.elastic.co/elasticsearch/elasticsearch:8.2.2
```

Copy the generated password and enrollment token and save them in a secure location. 

*You might need to scroll back a bit in the terminal to view the password and enrollment token.*

* Install and run Kibana

Kibana is the Web UI for managing various aspects of Elastic

It is important to run the following in **new** terminal window: 

```sh
docker pull docker.elastic.co/kibana/kibana:8.2.2
docker run --name kibana --net elastic -p 5601:5601 docker.elastic.co/kibana/kibana:8.2.2
```

A Unique link will be displayed in the terminal. 

1. Click & Open it in your browser, paste the enrollment token that you copied 
2. Log in to Kibana as the `elastic` user with the password that was generated when you started Elasticsearch. 

I created another use `dev` (as `Superuser`) in case I can't find the auto generated password for the elastic user (But please keep that one in a safe location 😄) 

-----

## Links

* [Setup Elasticsearch](https://www.elastic.co/guide/en/elasticsearch/reference/current/setup.html)
