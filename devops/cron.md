## Enable or create a cron.log file

* Edit the `/etc/rsyslog.d/50-default.conf` file. Find and uncomment the line starting with `#cron.*`
* Restart rsyslog service:

```sh
$ sudo systemctl restart rsyslog
## verify it ##
$ sudo systemctl status rsyslog 
```

* View the cron logs as `sudo tail -f /var/log/cron.log`

[*Reference*](https://www.cyberciti.biz/faq/howto-create-cron-log-file-to-log-crontab-logs-in-ubuntu-linux/)
