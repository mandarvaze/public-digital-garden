## Enable Basic Auth for some (or all) endpoints

See the [docs](https://docs.nginx.com/nginx/admin-guide/security-controls/configuring-http-basic-authentication/)

## Redirect all `http` traffic to `https`

Add following to the `server` block of the `nginx` configuration.

```
return 301 https://$server_name$request_uri;
```

In case you want only a few endpoints to redirect to https, rather than all the
traffic, you need to create two server blocks. One for `http` and other for
`https`.

`location` block under each determines which endpoint redirects to `https`.

See [this SO answer](https://stackoverflow.com/a/47533733/154947)

