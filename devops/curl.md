---
modified: 2024-10-29
---
## Get only the status code

`curl -s -o /dev/null -w "%{http_code}" https://www.example.com`

## Resumable downloads

`curl -LOC - url`
