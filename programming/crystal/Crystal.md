## REPL

There are two options:
* `crystal play` : This starts crystal playground locally accessible from http://127.0.0.1:8080
* `crystal i` : This is similar to `irb` available in Ruby
