# Why templates and static folder has subfolder named same as the app ?

Internally, Django combines all the `templates` folders across all the apps and creates a giant templates folder. So if we do not have appname (or something) under `templates` folder there may be clashes.
