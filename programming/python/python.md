## `pipenv`

### Handle timeouts

As of end-Nov 2018, creating a `lock` file takes **really** long time, and this
leads to `TIMEOUT` error (and `lock` file is NOT created)

There are two ways to handle this.

1. `pipenv install --skip-lock` This will, as the parameter suggests, will
   **not** create the lock file, thus ~~avoiding~~ delaying the issue.
2. Now you can create the `Pipfile.lock` via `pipenv lock`, but that would run
   into the same issue. So setup `PIPENV_TIMEOUT` environment variable before
   running the `pipenv lock` command. The value is in seconds. So for 3 minutes,
   set it to 180 (default is 120). Guessing how much to set it to is tricky. So
   if you are unsure, set it to some **really** high value like 10000

## logging

### formatter :

`%(asctime)s %(levelname)-5.5s [%(name)s:%(lineno)s][%(threadName)s]
%(message)s`

(Reference: Pyramid Logging tutorial)


## jinja2

* Use `{% %}` for Statements i.e. anything that is an instruction for jinja2
  * e.g. `if`, `from .. import ..`, `block`, `macro`
* Use `{{ }}` for Expressions i.e. Anything that renders the HTML output
* Add `-` before and/or after `%` to remove the white space.
  * Read more [here](https://jinja.palletsprojects.com/en/3.0.x/templates/#whitespace-control)
* Recursive `for` loop : Use keyword `recursive` with the `for` statement.
```html
<ul class="sitemap">
{%- for item in sitemap recursive %}
    <li><a href="{{ item.href|e }}">{{ item.title }}</a>
    {%- if item.children -%}
        <ul class="submenu">{{ loop(item.children) }}</ul>
    {%- endif %}</li>
{%- endfor %}
</ul>
```
* If a `macro` name starts with an underscore, it’s not exported and can’t be imported.

### Call python function from jinja

In order to make a python function available to jinja, you need to pass it to jinja while loading the Environment like :

```python

    def sum(a, b):
        return a+b

    env = Environment(loader=FileSystemLoader(path))
    env.globals['sum] = sum
```

Then in jinja call it like `{{ sum(3, 5) }}`

[Source](https://stackoverflow.com/a/6038550)

## async

### async iterators

* To make an object an async iterable, it must have an `.__aiter__()` method that 
returns an async iterator.
* The `.__anext__()` method must return an awaitable object. This can be a 
coroutine object or an object with an `.__await__(` method. 
* async comprehension use `async for`
```python
[item async for item in async_iter]
```
* We create awaitable objects by implementing the `.__await__()`
* asynchronous code runs in an event loop, which we start with the 
`asyncio.run()` function.


## Resources

* [Free Python Interview Course by Reuven Lerner](https://store.lerner.co.il/ace-python-interviews)
  * I have not watched this. But heard Reuven on some podcasts. He seems like a smart guy.
