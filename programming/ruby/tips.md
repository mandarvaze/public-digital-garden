## Make all strings immutable by adding 

Add `# frozen_string_literal: true` at the top of the file

[Reference](https://www.mikeperham.com/2018/02/28/ruby-optimization-with-one-magic-comment/)

## Always get clean prompt for `irb`

Create an alias in your shell so that `irb` automatically invokes `irb --sample-book-mode` (This is also called `--simple-prompt`)

## Decorator pattern in Ruby

```ruby
module Decorator
    def square(x)
        puts "I am going to square #{x}"
        super
    end
end

class Foo
    def square(x)
        puts x**2
    end
end

# let's create an instance
foo = Foo.new

# let's decorate the 'square' method on the instance
foo.extend Decorator

# let's invoke the new decorated method
foo.square(5) #=> "I am going to square 5"
              #=> 25
```
[Reference](https://stackoverflow.com/a/1892188/154947)

# `__END__`

> If you put an `__END__` in a Ruby file, everything after the `__END__` will be accessible as a file in the `DATA` variable. 

We used to do something similar in `C` 25 years ago. Combined code and data in a single file.
I don't remember specifics anymore, other than this fact.

*From Ruby Weekly Newsletter dated 03 Feb 2022*

-----
