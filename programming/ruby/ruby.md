
# Debugging

Ruby's Debugger seems to have improved with Ruby 3.1.
See this [README](https://github.com/ruby/debug) for details.

## Debug Gems

Set environment variable `EDITOR` like `"EDITOR=code"` or `"EDITOR=vim"` then if we run `bundle open <gem_name>`, it will open up our editor right to the directory of the installed gem itself.  (From Ruby Weekly Newsletter)

# Useful Gems

It is too early for me to make my own list (As of Jan 2022)
But [this](https://mindaslab.github.io/programming/web%20development/ruby/ruby%20on%20rails/2020/05/21/some-useful-gems-for-ruby-on-rails.html) post has good list (I think)

As of Feb 2022, I've started adding my own list 😆. Seriously, the above list is still valid.
I just wanted to start my own list of Gems that I "may" need in the future.

* [Easy receipts and invoices for your Rails applications](https://github.com/excid3/receipts)
* [Rodauth : Ruby's Most Advanced Authentication Framework](http://rodauth.jeremyevans.net/index.html)
