### Offline documentation

1. First fetch documentation for offline use : `mix hex.docs fetch PACKAGE [VERSION]`
1. Open the offline version in documentation `mix hex.docs offline elixir`
1. See more [here](https://hex.pm/docs/tasks)

### Elixir and Blockchain

* [A high level Ethereum JSON RPC Client for Elixir](https://github.com/hswick/exw3/)
* [exthereum](https://github.com/exthereum) - This is an umbrella project.

### Publish your own hex package

Sometime ago, I published my first (and only - so far) hex package called [`qq`](https://hex.pm/packages/qq)

Inspired by [`q.q`](https://github.com/zestyping/q) and similarly this [one](https://github.com/y0ssar1an/q)

I too wanted to name it as just `q` but it was rejected as being too short for hex package,
hence `qq`

References used : [this medium article](https://medium.com/blackode/how-to-write-elixir-packages-and-publish-to-hex-pm-8723038ebe76) and [this blog](http://learnwithjeff.com/blog/2015/10/28/your-first-hex-package/)

##  `mix check`

Add the following to the `aliases` section of `mix.exs`
```elixir
check: [
        "compile --warnings-as-errors",
        "format --check-formatted",
        "credo --strict",
        "coveralls.html",
        "dialyzer --format short"
      ]
```

Then run `mix check`

*This is much like `make lint` I run on my python projects*

[Source](https://elixirforum.com/t/do-you-use-formatter-in-your-elixir-project/39638/13)
## Resources

* [Elixir Cheatsheet](https://devhints.io/elixir)
