* UUID primary keys using Ecto. See [this](https://blog.fourk.io/uuids-as-primary-keys-in-phoenix-with-ecto-and-elixir-1dd79e1ecc2e)
* Auto timestamps: Include `timestamps` in the schema to automatically generate `inserted_at` and `updated_at` columns to your tables
* Unique constraint
    * [hashrocket](https://til.hashrocket.com/posts/977c254181-unique-indexes-with-ecto)
    * [elixir forum](https://elixirforum.com/t/unique-constraint-migration/1950)
    * [Stack overflow](https://stackoverflow.com/questions/32460920/making-a-field-unique-in-ecto)
    * From [Ecto docs](https://hexdocs.pm/ecto/Ecto.Schema.html#timestamps/1)
