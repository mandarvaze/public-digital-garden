---
created: 2021-07-31
modified: 2023-06-22
---

# Issues (and workarounds)

* [No password prompt for Wi-Fi](https://github.com/regolith-linux/regolith-desktop/issues/413)
* [VPN does not work](https://github.com/regolith-linux/regolith-desktop/issues/64). I don't use VPN, but the discussion is interesting.

## Created `EFI boot` partition on MBP

This leads to me unable to boot into macOS, unless I press <kbd>Option</kbd> button during startup and explicitly select the `Macbook Pro SSD` disk to boot from.
Without that I get `grub>` prompt which seems broken.

### Solution

Get the correct identifier using `diskutil list`. Look for type `EFI`.

```sh
$ sudo mkdir /Volumes/EFI
$ sudo mount -t msdos /dev/disk0s1 /Volumes/EFI
$ cd /Volumes/EFI/EFI  # EFI folder inside EFI volume
$ rm -rf ubuntu/ BOOT/
$ cd # Back to home, at least `cd` away from /Volumes/EFI, else it can't be unmounted
$ diskutil unmount /Volumes/EFI
```

[Source](https://www.macobserver.com/tips/quick-tip/macos-removing-windows-efi-boot-entry/)

## Unable to boot into regolith

I installed regolith on external HDD, but when I select that HDD to boot from, all I get it `grub>` prompt.

Solution : None yet 😢
