## Notes from running Manjaro linux on MBP (early 2015)

* ✅ Run in docked mode. (laptop lid closed, external monitor ON)
  * When using KDE plasma version, this can be done easily via "Energy Settings" option
  * Choose `Do nothing` option for `Lid Closed`
  * Can also be done for XFCE by updating `/etc/systemd/logind.conf` (I did not have success)
  * Uncomment `HandleLidSwitch` option (Google for exact details)
* Install Hyper Terminal
  * ❌ Using `pamac` UI or enabling `AUR` did not help. It failed eventually
    * To be fair, they suggested `aurman` which I did not use.
  * ✅ Using AppImage downloaded from their [website](https://hyper.is/) worked
  * Also installed `nord-hyper` theme
  * <kbd>Ctrl+,</kbd> to open config file

