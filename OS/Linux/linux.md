---
modified: 2024-11-02
---

# Backup

* [Borg Backup](https://borgbackup.readthedocs.io/en/stable/index.html) 
  * Supports "data deduplicated backup" (Or what we used to call differential backup). So daily backups only backup what has changed.
  * Back be backed up to local filesystem or remote
  * Supports encryption

# Fonts

* Install msttfcorefonts using appropriate command for your distro
* Select appropriate fonts (after installing Microsoft fonts) in Firefox
  * In Settings, search for Fonts, and use `Advanced` option to set fonts
  * Reduce font size if needed
* Depenbing on your DE there is an option to set the fonts in Linux UI as well
  * It was `Noto Sans` which I changed to `Arial`

## Command Line

* Format a disk as exFAT : `sudo mkfs.exfat -n LABEL /dev/sdXn`
  * On [[OpenSuse]] you may need to install `exfat.utils` using `sudo zypper install exfat-utils`
  * For other distro install appropriate package
* `sudo fdisk -l` to list the disks. Useful in previous command if you are unsure of the device

[[Manjaro]]
[[Ubuntu]]
[[Regolith]]
[[OpenSuse]]
