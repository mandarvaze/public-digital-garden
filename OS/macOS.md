---
modified: 2024-03-20
---

# Show hidden files in Finder

<kbd>cmd + shift + period</kbd>

# Set external screen as a primary display

System Preferences -> Display -> Arrangement

When we configure two displays, we get to see a white bar on top of one of the display.
Who knew that one can drag this white bar ? Let alone that it indicates the primary display.

Drag that white bar to the display that you wish to designated as primary display. :tada:

# Type Marathi

The trick is to add `Devnagary - QWERTY` language, rather than `Marathi` (or `Hindi`)
This allows one to type `s` and get स - this is transliteration (and is easy to use).
Using native marathi (or hindi) means having to learn a new keyboard layout.
e.g. in Native marathi Keyboard (on macOS) you have to type `m` to get स 😄

# Update keyboard shortcut for Lock Screen

* Open System Preferences
* Open "Keyboard" preference pane
* Go to "Shortcuts" tab
* Select "App Shortcuts"
* Click "+"
* Select "All Applications". In the "Menu Title" field type "Lock Screen" and press your shortcut
* Quit System Preferences

[Ref](https://apple.stackexchange.com/a/336408)

# Update location where screenshots are saved

* `Command + Shift + 5`
* Select 'Other Location'
* I create `~/screenshots` just for this purpose 😄
