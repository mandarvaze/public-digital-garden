# Add new paths

Run `fish_add_path` from the terminal. This affects existing and future sessions (which is what you want most of the times)

```sh
$ fish_add_path /my/new/path
```

It is unclear whether we should add this to `config.fish` file. Depending on the version you are using, adding to `config.fish` may get added every time and `PATH` will grow unnecessarily long.
Apparently, later version fix this. 

# Using `nvm` with `fish` shell

Most programs can not understand shims set by frameworks like `nvm` (or `pyenv`) In this specific case, VSCode could not find `npm` and I kept getting `ENOENT` errors.

Solution: `set --universal nvm_default_version latest`

