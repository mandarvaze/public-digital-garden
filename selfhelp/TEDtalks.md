# [Procrastination is Power](https://www.youtube.com/watch?v=GPfB6K-03dU)

* Our brain makes us procrrastinate to keep us safe for  things (it thinks) are bad for us.
* Brain only understands Pleasure or Pain
  * More Pleasure
  * Avoid Pain
* Make your brain do things (that you procrastinate about) by showing either
  * Future pain due to not doing the thing now or
  * Future pleasure due to doing the thing now (Reward after doing unpleasant thing)

# [Secret to Changing Negative Self Talk](https://www.youtube.com/watch?v=HcRcr8m_WPc)

Awesome Acronyms

* STRONG :  **S**tand **T**all, **R**emain **O**ptimistic, **N**ow, **G**o get it
* GREAT : **G**et **R**eady, **E**xpect **A** **T**errific Day
* YES I CAN : **Y**ou **E**xpect **S**uccess **I**n **C**hallenging **A**ctivities **N**ow
* CHANGE : **C**ircumstances **H**ave **A**ltered, **N**ow **G**et **E**ngaged 

I had already read this one for FEAR
* FEAR : **F**alse **E**vidence **A**ppearing **R**eal

But this one was new to me.
* FEAR : **F**uels **E**nergy **A**nd **R**esilience

* I CAN : **I**nspired **C**onfident **A**ffirmed, **N**ever a Doubt
* PUSH : **P**ut **U**p **S**ome **H**ard work
