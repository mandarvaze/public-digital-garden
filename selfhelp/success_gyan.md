## Thaddeus Lawrence

> When you are consistent with your actions, your success will appear

Stop Outsourcing
- Agency (Ability to act)
- Authority
- Approval

## Puja

- Doubt and Fear
- Overwhelm
- Procrastination
- Perfectionism

## Dr. Meghana Dikshit

- Procrastination is not your fault
- Speed Limiter = Unconscious expectation set point
- Emotions can make you successful or failure
- Habits
- Actions


> Setting Goal Conscious Process
> Achieving Goal is Unconscious Process

> Are you interested or committed ?

Interest may be lost over time


## Siddharth Rajsekar

How to find your money making micro niche in 10 minutes

- Pick One Problem you want to Solve
  - Business
  - Career
  - Health
  - Money
  - Relationship
  - Creativity
- Specific Area to create an impact (Deep)
  - Specialist Vs Generalist
- Pick a method/tool to Solve the problem (Vehicle)
- Validate Niche using 5P system
  - Passion
  - Problem (Should solve Problem, else it is a hobby)
  - Persona (Target Micro Market)
  - Potential
  - Payment (Profits)
- Convert this into a Mission
- Build into Knowledge Business
  - Course
  - Coaching (1:1)
  - Consulting
  - Collaborations
