These are the notes from Udemy Course *Neuroplasticity: Rewire your Brain*

# Summary

* Brains "grows" when learning new things.
* After a while these "new" things become second nature and go on auto pilot
* Create *Enriched Environments* to keep brain growing


# Exercise to Simulate Brain

## Sensorial/Motoric 

* Close eye, and allow tactile senses to take over
  * Close your eye during (entire duration of) the shower
  * (Only) start the car when eyes closed
* Switch Hands (Use opposite hand for activities done during the day)
  * Brush Teeth
  * Shave
  * Write
  * Mouse
  * Comb hair
  * Open Door
  * Watch on the other wrist
  * **Extra Credit:** Use only one hand, preferably non-dominant one, for daily activities

## Social

## Congitive

# Neuroplasticity Paradox

Parts of the brain that are not used, shrink (*Use it or lose it*)

But other parts of the brain expand to "fill in that gap"

Brain is a prime real estate. No part remains unoccupied for long.

Most used parts become bigger and stronger **that is why it is difficult to break old habits**

At younger age, brain is malleable (all parts of brain are developing equally) hence kids pickup new technology easier than elders. (Cause their brain is already occupied by other habits they've done over decades)

## Structural changes Exercises

* Learn to play a new instrument
* Learn touch typing (Use 10 fingers to type) 
  * I thought I did , but turns out I use at max 6 fingers. I noticed I don't use pinky and the finger next to it at all
* Gardening : Make you use all (most?) or your senses
* Volunteer your time at local charity
* Join Amateur Theater Group and become an actor
* Learn new thing - from your field or new altogether
* Learn a new language
  * Learn Sign Language, as it involves tactile sense
* Keep a diary

> Repetition is the mother of all learning

## New Habits

* Linking your behavior to your intrinsic motivation makes it enjoyable and thus sustainable.
  * Create list of things that one enjoys
  * Link them to new habit 
  * i.e. Do them together (e.g. Exercise while listening to the podcast/music. Go to the Gym w/ Friend etc.)
* Habit = Cue -> Routine/Action -> Reward

## Change bad habits

* Changing cue (If you see Junk food, you eat, even if you aren't hungry. So lock away junk food, better still don't buy it altogether)
* Determine the reward linked to the habit, change action associated w/ but same reward.
* Do the new habit right after existing habits 

> Increase willpower/self control with baby steps

Rather than quitting cold turkey, take small actions to extert self control.

## Unconscious drivers

* genetics
* Life experiences
* environment/surroun/context

## Types of Procrastinators

* Avoiders
  * Brain telling us to avoid *perceived* dangers (e.g. Unpleasant phone conversation)
* Freezers
  * Fight, Flight/Flee or Freeze
* Non-motivated
  * Task that is not in their field of intrinsic motivation
(*There are six more types like addicts, perfectionists, thrill-seekers, Rebels, Low Esteem, Equalizers*)

## Willpower

* Willpower does not fix procrastination, because it does not address the root cause
* Temptation > Willpower (for procrastinators)

> People who are good at self-control seem to be structuring their lives in a way to avoid having to make a self-control

## Strategies to beat procrastination

### Domapine based

* Visualization
* Gamify
* Small to-do list
* Motivation Bundle (Do the task-you-avoid with activity you like)
* Look at the bright side (Rather than focusing on downsides that make you avoid the task)

### Contextual

* Team up
* No temptation/clean desk
* Buddy System
* Surround yourself with "Action Takers"
* Change settings (Work from different place - change of scenery may mean old habits won't get the Cue they need to start procrastinating)

### Brain hacks

* Covert Start (Just open the document, Just type one sentence, then paragraph.. before you know you are updating the document)
* Pay attention to words
  * I really *should* be working. Vs
  * I am working right now 
* Third Person (Name the procrastinator - like "Dude" and "Talk to him" when he holds you back)
  * (*Personal comment: I'm not really sure about this*)


## Memory 

* Brain often cannot tell the difference between real memory and a false one.
* Body often reacts to a false memory just as it would to a real one. (Just thinking about exercise has positive effect on the muscles)
* Use this to lessen the negative emotional effect of the past traumatic events
  * Imagine Alternate Ending
  * Reframing
    * Identify that there is no intrinsic value from recalling the past event
    * "Why am I hurt?"
    * Evidence on the contrary
    * Find a different perspective
