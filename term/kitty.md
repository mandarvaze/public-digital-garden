### Kitty

Default Kitty is also good. But there is so much to explore.

* Windows

Kitty has concept of Windows, which are panes, and **OS Windows**.
`Cmd+n` will create new **Os Window** which I have been using for a long time. But switching between OS windows is painful.

I had expected commands for vertical split and horizontal split, but there aren't. Instead, there are Layouts.

* One creates a new "pane" via `Ctrl+Shift+Enter`
* Move between panes using `Ctrl+Shift+[` (Or `]` to move in the other direction)
* Resize the panes using `Ctrl+Shift+r` (Then follow on-screen instructions)
