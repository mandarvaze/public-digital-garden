# Publishing

* [Perfect Edition](https://github.com/robinsloan/perfect-edition) : 
  * a web e-book template that works in modern browsers
  * a tool to generate a nice web edition and robust EPUB edition from the same text
  * suitable for fiction and other straight-through prose
* [Softcover](https://www.softcover.io) : Used for Ruby on Rails manual.
* [LeanPub](https://leanpub.com/authors) 
  * 80% Royalties
  * Permission to publish elsewhere at the same time.
  * Submit Markdown. LeanPub platform creates other formats like PDF, epub etc.
* Use Asciidoc to publish to different formats like EPUB3
  * [How to structure the manuscript.](https://docs.asciidoctor.org/epub3-converter/latest/#structuring-your-manuscript)
