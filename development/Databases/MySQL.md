### Backup

In order for the backup to work, you need to provide `LOCK TABLES` permission.

`GRANT LOCK TABLES ON `dbname`.* TO 'backupuser@'hostname;`


### Restore the data from the backup

`mysql -u username -p database < mysql_dump.sql`

You will be asked for the password interactively.

### See user permissions

```
$ mysql -u username -p

mysql> show grants;
+-------------------------------------------------------------------------------------------------------------------------+
| Grants for username@localhost                                                                                   |
+-------------------------------------------------------------------------------------------------------------------------+
| GRANT USAGE ON *.* TO 'username@'localhost' IDENTIFIED BY PASSWORD '*1DCC9264F13F90CDA7F67C18BE6DF06FCF436498' |
| GRANT ALL PRIVILEGES ON `dbname`.* TO 'username@'localhost' WITH GRANT OPTION                       |
+-------------------------------------------------------------------------------------------------------------------------+
2 rows in set (0.00 sec)
```
