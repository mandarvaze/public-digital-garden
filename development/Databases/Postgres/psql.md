## Easily view null Values

By default, `psql` uses whitespace to show `null` values. I have had tough time *visually* identifying null data from data containing actual whitespace.

Turns out one can use `\pset null`.

Josh Branchaud [suggests](https://github.com/jbranchaud/til/blob/master/postgres/a-better-null-display-character.md) `\pset null 'Ø'`

## Turn the pager off

When viewing a lot of records, pager can hinder your progress. You would rather not see the paginated output. `pset` to the rescue (again)

`\pset pager off`

There are additional options discussed [here](https://serverfault.com/questions/235184/turn-off-pager-for-psqls-interactive-output)

## Change output format to CSV

`\pset format csv` on `psql` prompt

## Make these defaults (if you want)

If you see yourself using the above repeatedly, why not add them to `~/.psqlrc` ?

## Clear screen in `psql`

If you are on Linux/macOS use `\! clear`, on Windows `\! cls` 

Essentially `\!` is used to execute any of the shell command inside `psql`. I tried `\! ls -l` for fun, and it worked!