### Backup
`/usr/bin/mongodump -h $HOST -d $DBNAME -o $DEST --password $PASSWORD --username $USERNAME`

### Restore
`mongorestore --host {db.example.com} --db {my-db} {db-name}/`

### Reference:

1. [mongodump](https://docs.mongodb.com/manual/reference/program/mongodump/#bin.mongodump)
1. [mongorestore](https://docs.mongodb.com/manual/reference/program/mongorestore/#bin.mongorestore)
1. [MongoDB backup to and restore from S3](https://gist.github.com/eladnava/96bd9771cd2e01fb4427230563991c8d)
