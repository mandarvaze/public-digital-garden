## Side by side diffs

Sometimes it is nice to see the diffs side by side, rather than default one below another. When you need this :

```sh
$ diff -y file1 file2
```

## Template for creating an issue/ticket

I picked this up during my assignment with [Niteo](https://github.com/niteoweb). 🙏 

```markdown
As a `<rolename>`,
I want to `<what do you want?>`
So that user `<what is the benefit of this change>`

**Problem**

`<explain the problem in detail>`

**Proposal**

`<proposed solution goes here. If it warrants a separate document, may be add a reference here. Most of the time, especially for bugs, separate document is not required. So the design can go here>`

**Pitfalls**

`<What to watch out for>`

**Best practices**

Before we mark this as done, following should be complete. Add/Remove following as appropriate

- [ ] Documentation is revised:
- [ ] Support engineers are informed of the manual nature of the fix.
- [ ] Appropriate tests are added.

```

## Regular Expression Builder

[regex101](https://regex101.com/) is an awesome resource. It lets you build and test your regular expression. It has a nice cheat sheet, option to select the programming language in which you are gonna use the regex.

Finally it lets you create an entire sample code based on the regex you built (and the test string you used) 
